#  cmake开发手册中文版
官方版本：https://cmake.org/cmake/help/latest/

翻译的官方版本：`3.26.4`

### 译者注
翻译：ZX  
博客：https://www.cnblogs.com/zx-admin/  
翻译问题反馈 QQ: 903464207


### 一些其他资料
cmake wiki：https://gitlab.kitware.com/cmake/community/-/wikis/home

Cmake中文实战教程： https://brightxiaohan.github.io/CMakeTutorial/

CMake Cookbook中文版：https://www.bookstack.cn/read/CMake-Cookbook/README.md

Modern CMake 简体中文版：https://www.bookstack.cn/read/Modern-CMake-CN-Modern-CMake-zh_CN/08ad6f6237beace2.md

CMake 保姆级教程（上）：https://subingwen.cn/cmake/CMake-primer/  
CMake 保姆级教程（下）：https://subingwen.cn/cmake/CMake-advanced/

## 介绍
CMake是一个管理源代码构建的工具。最初，CMake被设计为各种方言Makefile的生成器，今天CMake生成现代构建系统，如Ninja，以及ide(如Visual Studio和Xcode)的项目文件。

CMake广泛用于C和c++语言，但它也可以用于构建其他语言的源代码。

第一次接触CMake的人可能有不同的初始目标。要了解如何构建从互联网下载的源代码包，请从用户交互指南开始。这将详细介绍运行`cmake(1)`或`cmake-gui(1)`可执行文件所需的步骤，以及如何选择生成器，以及如何完成构建。

使用依赖指南针对的是希望开始使用第三方库的开发人员。

对于使用CMake开始项目的开发人员来说，`CMake教程`是一个合适的起点。`CMake -buildsystem(7)`手册旨在扩展开发人员维护构建系统的知识，并熟悉可以在CMake中表示的构建目标。`cmake-packages(7)`手册解释了如何创建可以被第三方基于cmake的构建系统轻松使用的包。

## 使用说明
采用gitbook编写，建议要自己导出html或者pdf，使用node版本`v10.24.1`，过新的版本会导致编译出错，多版本建议是用`nvm`

GitBook生成PDF依赖`calibre`

下载3.x版本，否则在gitbook pdf的过程中会卡住  
打开3.48.0下载地址: https://download.calibre-ebook.com/3.48.0/

如果还是不能导出pdf，就重启一下，可能是`环境变量`未重新加载

```
### 安装
$ npm install gitbook-cli -g

### 安装插件
$ gitbook install

### 以http服务访问
$ gitbook serve

### 编译成html方式发布
$ gitbook build

### 导出pdf
$ gitbook pdf
```


## 目录

* [命令行工具]()
    * [cmake(1)](chapter_1/cmake.1.md)
    * [ctest(1)](chapter_1/ctest.1.md)
    * [cpack(1)](chapter_1/cpack.1.md)
* [交互对话工具]()
    * [cmake-gui(1)](chapter_2/cmake-gui.1.md)
    * [ccmake(1)](chapter_2/ccmake.1.md)
* [参考手册]()
    * [cmake-buildsystem(7)](chapter_3/cmake-buildsystem.7.md)
    * [cmake-commands(7)](chapter_3/cmake-commands.7.md)
    * [cmake-compile-features(7)](chapter_3/cmake-compile-features.7.md)
    * [cmake-configure-log(7)](chapter_3/cmake-configure-log.7.md)
    * [cmake-developer(7)](chapter_3/cmake-developer.7.md)
    * [cmake-env-variables(7)](chapter_3/cmake-env-variables.7.md)
    * [cmake-file-api(7)](chapter_3/cmake-file-api.7.md)
    * [cmake-generator-expressions(7)](chapter_3/cmake-generator-expressions.7.md)
    * [cmake-generators(7)](chapter_3/cmake-generators.7.md)
    * [cmake-language(7)](chapter_3/cmake-language.7.md)
    * [cmake-modules(7)](chapter_3/cmake-modules.7.md)
    * [cmake-packages(7)](chapter_3/cmake-packages.7.md)
    * [cmake-policies(7)](chapter_3/cmake-policies.7.md)
    * [cmake-presets(7)](chapter_3/cmake-presets.7.md)
    * [cmake-properties(7)](chapter_3/cmake-properties.7.md)
    * [cmake-qt(7)](chapter_3/cmake-qt.7.md)
    * [cmake-server(7)](chapter_3/cmake-server.7.md)
    * [cmake-toolchains(7)](chapter_3/cmake-toolchains.7.md)
    * [cmake-variables(7)](chapter_3/cmake-variables.7.md)
    * [cpack-generators(7)](chapter_3/cpack-generators.7.md)
* [指南]()
    * [CMake教程](chapter_4/tutorial.md)
    * [用户交互指南](chapter_4/user-interaction.md)
    * [使用依赖指南](chapter_4/using-dependencies.md)
    * [导入和导出指南](chapter_4/importing-exporting.md)
    * [IDE集成指南](chapter_4/ide-integration.md)
* [发布说明]()
    * [CMake发行说明](chapter_5/release.md)
* [索引和检索]()
    * [索引](chapter_6/genindex.md)
    * [搜索页面](https://cmake.org/cmake/help/latest/search.html)

